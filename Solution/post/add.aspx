﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer.master" AutoEventWireup="true" CodeFile="add.aspx.cs" Inherits="post_add" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
     <i class="fa fa-plus"></i> Add Post
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <form runat="server" class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="control-label col-lg-4">Post Date</label>
                <div class="col-lg-8">
                    <asp:TextBox ID="txtPDate" runat="server" class="form-control" type="date" required />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Post Type</label>
                <div class="col-lg-8">
                    <asp:DropDownList ID="ddlPType" runat="server" class="form-control" required >
                            <asp:ListItem Text="Option 1" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Option 2" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Option 3" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Title</label>
                <div class="col-lg-8">
                    <asp:TextBox ID="txtTitle" runat="server" class="form-control text-capitalize" MaxLength="80" required />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Post</label>
                <div class="col-lg-8">
                    <CKEditor:CKEditorControl ID="ckPost" runat="server"></CKEditor:CKEditorControl>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="control-label col-lg-4">Featured Image</label>
                <div class="col-lg-8">
                    <asp:FileUpload ID="fuImage" runat="server" class="form-control" required />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Keyword</label>
                <div class="col-lg-8">
                    <asp:TextBox ID="txtMunicipality" runat="server" class="form-control text-capitalize" MaxLength="100" required />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Status</label>
                <div class="col-lg-8">
                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" required>
                        <asp:ListItem Text="Published" Value="0"/>
                        <asp:ListItem Text="Draft" Value="1" />
                    </asp:DropDownList>

                </div>
            </div>
             <div class="form-group">
                <div class="col-lg-offset-4 col-lg-8">
                    <asp:Button ID="btnAdd" runat="server"
                        class="btn btn-success" Text="Add" />
                </div>
            </div>
                </div>
            </div>
    </form>
</asp:Content>

